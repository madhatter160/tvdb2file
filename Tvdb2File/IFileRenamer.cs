﻿using System;
using System.Collections.Generic;

namespace Tvdb2File
{
   public interface IFileRenamer
   {
      event EventHandler<FileRenamedArgs> FileRenamed;
      event EventHandler<FileRenameFailedArgs> FileRenameFailed;

      int Rename( string seasonDirectoryPath, IList<Episode> episodeList, bool dryRun );
   }
}
