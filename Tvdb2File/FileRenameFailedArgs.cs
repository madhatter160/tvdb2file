﻿using System;

namespace Tvdb2File
{
   public class FileRenameFailedArgs : EventArgs
   {
      public string FileName
      {
         get;
         private set;
      }

      public Exception Error
      {
         get;
         private set;
      }

      public FileRenameFailedArgs( string fileName, Exception error )
      {
         this.FileName = fileName;
         this.Error = error;
      }
   }
}
