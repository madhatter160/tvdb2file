﻿using System;
using System.Runtime.Serialization;

namespace Tvdb2File
{
   [Serializable]
   public class EpisodeFileMatchNotFoundException : Exception
   {
      //
      // For guidelines regarding the creation of new exception types, see
      //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
      // and
      //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
      //

      public EpisodeFileMatchNotFoundException()
      {
      }

      public EpisodeFileMatchNotFoundException( string message )
         : base( message )
      {
      }

      public EpisodeFileMatchNotFoundException( string message, Exception inner )
         : base( message, inner )
      {
      }

      protected EpisodeFileMatchNotFoundException( SerializationInfo info, StreamingContext context )
         : base( info, context )
      {
      }
   }
}
