﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Tvdb2File
{
   public class PatternFileRenamer : IFileRenamer
   {
      public event EventHandler<FileRenamedArgs> FileRenamed;
      public event EventHandler<FileRenameFailedArgs> FileRenameFailed;

      public int Rename( string seasonDirectoryPath, IList<Episode> episodeList, bool dryRun )
      {
         var episodeFilePaths = Directory.GetFiles( seasonDirectoryPath, "*.mp4", SearchOption.TopDirectoryOnly );

         for ( var episodeFileNamesIndex = 0; episodeFileNamesIndex < episodeFilePaths.Length; episodeFileNamesIndex++ )
         {
            var episodeFilePath = episodeFilePaths[episodeFileNamesIndex];
            var sourceFileName = Path.GetFileName( episodeFilePath );

            var episodeMatch = episodeList.FirstOrDefault( ( episode ) => DoesFileNameMatchEpisode( sourceFileName, episode ) );

            if ( episodeMatch == null )
            {
               FileRenameFailed?.Invoke( this, new FileRenameFailedArgs( episodeFilePath, new EpisodeFileMatchNotFoundException() ) );
               continue;
            }

            var targetFileNameWithExt = ConstructEpisodeFileName( episodeMatch ) + Path.GetExtension( episodeFilePath );
            var targetFilePath = Path.Combine( Path.GetDirectoryName( episodeFilePath ), targetFileNameWithExt );

            if ( !dryRun && !File.Exists( targetFilePath ) )
            {
               // TODO: Uncomment when done testing!!
               File.Move( episodeFilePath, targetFilePath );
            }

            this.FileRenamed?.Invoke( this, new FileRenamedArgs( Path.GetFileName( episodeFilePath ), targetFileNameWithExt ) );
         }

         return episodeFilePaths.Length;
      }

      private bool DoesFileNameMatchEpisode( string episodeFileName, Episode episode )
      {
         var episodeName = this.NormalizeEpisodeName( episode.NameBase );
         var fileName = this.NormalizeEpisodeName( episodeFileName );

         // Sample episode file names that will match the pattern below.
         // The 6th (i=5) group will be the episode name.
         //
         // S01E01 Episode Name.mp4
         // S01E01 - Episode Name.mp4
         // S01E01 - 1 Episode Name.mp4
         // S01E01 - 1 - Episode Name.mp4
         // S01E01 Episode Name Part 1.mp4
         // S01E01 - Episode Name Part 1.mp4
         // S01E01 - 1 Episode Name Part 1.mp4
         // S01E01 - 1 - Episode Name Part 1.mp4
         var episodePattern = @"([Ss]\d{1,2}[Ee]\d{1,2}((-\d)?( -)?)? )(" + episodeName + ")([^.]{0,}).[Mm][Pp]4";

         var episodeRegex = new Regex( episodePattern );

         var match = episodeRegex.Match( fileName );

         return match.Success;
      }

      private string ConstructEpisodeFileName( Episode episode )
      {
         var invalidFileNameChars = Path.GetInvalidFileNameChars();
         var name = new StringBuilder( episode.NameBase );

         foreach ( var c in invalidFileNameChars )
         {
            name = name.Replace( c, '_' );
         }

         return $"S{episode.SeasonNumber:D2}E{episode.EpisodeNumber:D2} {name}";
      }

      private string NormalizeEpisodeName( string name )
      {
         var normalized = new StringBuilder( name );
         var invalidFileNameChars = Path.GetInvalidFileNameChars();

         for ( var i = 0; i < normalized.Length; i++ )
         {
            if ( !char.IsUpper( normalized[i] ) )
            {
               normalized[i] = char.ToUpper( normalized[i] );
            }
            else if ( invalidFileNameChars.Any( c => c == normalized[i] ) )
            {
               normalized[i] = '_';
            }
            else if ( char.IsPunctuation( normalized[i] ) )
            {
               normalized[i] = '-';
            }
         }

         return normalized.ToString();
      }
   }
}

