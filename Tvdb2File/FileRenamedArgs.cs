﻿using System;

namespace Tvdb2File
{
   public class FileRenamedArgs : EventArgs
   {
      public string OldName
      {
         get;
         private set;
      }

      public string NewName
      {
         get;
         private set;
      }

      public FileRenamedArgs( string oldName, string newName )
      {
         this.OldName = oldName;
         this.NewName = newName;
      }
   }
}
